## 変数設定
DB_NAME=${1}
BASE_DIR=/objchecktool/check_obj_report/result
DIR1=${BASE_DIR}/${2}
DIR2=${BASE_DIR}/${3}

## 引数チェック
if [ ${#} -ne 3 ];then
  echo "引数の数が足りません。"
  echo "第1引数：DB名"  
  echo "第2引数：現行環境check_obj_report結果配置ディレクトリ（例：before_YYYYMMDD_GENESIS）"
  echo "第3引数：新環境check_obj_report結果配置ディレクトリ（例：after_YYYYMMDD_GENESIS）"
  exit 1
fi

## ディレクトリチェック
if [ ! -d ${DIR1} ];then
  echo "該当のディレクトリ(${DIR1})がありません。処理を終了します。"
  exit 1
fi

if [ ! -d ${DIR2} ];then
  echo "該当のディレクトリ(${DIR2})がありません。処理を終了します。"
  exit 1
fi

## 処理開始
for file_name in `ls ${DIR1}`
do
echo -n 【${file_name}】
inum=`diff -y -W 150 --suppress-common-lines ${DIR1}/${file_name} ${DIR2}/${filename} | wc -l`
if [ ${inum} -eq 0 ] ;then
  echo " ⇒ 差分なし"
else
  echo " ⇒ 差分あり"
fi
diff -y -W 300                               ${DIR1}/${file_name} ${DIR2}/${filename} | head -n 1
diff -y -W 300 --suppress-common-lines       ${DIR1}/${file_name} ${DIR2}/${filename}
echo ""
done
